#define _USE_MATH_DEFINES

#include "functions/functions.h"
#include <iostream>
#include <cmath>
#include <iterator>

int main(){

	int k;
	int thread_numb;
	int process_numb;
	int start_offset;

	//basic perm info
	std::cout << "Enter the k length of the superpattern\n";
	std::cin >> k;

	//multithreading info
	std::cout << "Enter the number of threads processing(>=1)\n";
	std::cin >> thread_numb;

	if (thread_numb - 1){

		std::cout << "Enter this process number\n";
		std::cin >> process_numb;
		std::cout << "Enter offset to start if needed\n";
		std::cin >> start_offset;

	}else{
		process_numb = 0;
		start_offset = 0;
	}

	//bounds for length of superpattern
	int start = ceil( (pow(k , 2) + 1 ) / 2 );
	int final = ceil( 1.000076*pow(k , 2 ) / pow(M_E , 2) );

	bool perm_check = true;
	std::vector<int> final_perm;
	std::vector<int> basicPerm;
	std::unordered_set<std::vector<int>> setus_deletus;

	for (int n = (start - start_offset); n >= final ; n--){
		int counter = 0;
		
		basicPerm.clear();
		for (int i=1; i<=n; i++){
			basicPerm.insert(basicPerm.end(),i);
		}

		while(std::next_permutation(basicPerm.begin(),basicPerm.end())){

			if (counter%thread_numb == process_numb){

				setus_deletus = paternize(basicPerm,k);

				perm_check = superpattern_check(setus_deletus,k);

				if ( perm_check ){
					final_perm = basicPerm;
					std::cout << n << " first superpattern = [ ";
					std::copy(final_perm.begin(), final_perm.end(), std::ostream_iterator<int>(std::cout, " "));
					std::cout << "]\n";
					break;
				}
			}
			counter += 1;
		}

		if ( !perm_check ) {
			std::cout << k <<" smallest perm is [ ";
			std::copy(final_perm.begin(), final_perm.end(), std::ostream_iterator<int>(std::cout, " "));
			std::cout << "]\n";
			break;
		}
	}
	return 0;
}