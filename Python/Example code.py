import math
import itertools

k = int(input("Enter the k length of the superpattern\n"))

thread_numb = int(input("Enter the number of threads processing(>=1)\n"))

if (thread_numb - 1):
	process_numb = int(input("Enter this process number\n"))
	start_offset = int(input("Enter offset to start if needed\n"))

else:
	thread_numb = 1
	process_numb = 0
	start_offset = 0

start = math.ceil((k**2+1)/2)

final = math.ceil(k**2/((math.e)**2))-1

kPerms = set(itertools.permutations(range(1,k+1),k))

#order the patterns
#perm is a tuple
def order(perm):
	ordered_perm = sorted(perm)
	perm1 = list(perm[:])
	for x in range(len(perm)):
		perm1[perm1.index(ordered_perm[x])] = x+1
	return tuple(perm1)

#turn an array in various patterns and associates with respective ordenation
def paternize(array,k):

	dicc = {}

	for perm in itertools.combinations(array,k):
		dicc[perm] = order(perm)

	return dicc

#check if an array is a superpattern
def superpattern_check(array,kperms,k):
	bool0 = False

	set0 = set(paternize(array,k).values())

	for perm in kperms:

		if not (perm in set0):
			bool0 = True

	return not bool0


for n in range((start - start_offset), final , -1):
	print("Current length =",n)
	fac = math.factorial(n)
	counter = 0
	for perm in itertools.permutations(range(1,n+1)):

		if counter%thread_numb == process_numb:

			perm_check = superpattern_check(perm,kPerms,k)

			if perm_check:
				final_perm = perm
				print(str(n),"first superpattern =",final_perm)
				break

		counter += 1

	if not perm_check:
		print(str(n+1)+" smallest perm is "+str(final_perm))
		break
